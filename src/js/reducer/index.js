import {combineReducers} from 'redux'
import testReducer from "../../Components/TestSecondComponent/redux/reducers";

const reducers = combineReducers({
    testReducer
});

export default reducers;
