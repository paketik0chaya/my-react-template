import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Provider} from "react-redux";
import TestFirstComponent from "./Components/TestFirstComponent";
import store from "./js/store";
import TestSecondComponent from "./Components/TestSecondComponent";

const App = () => (
    <Provider store={store}>
        <Router>
            <div>
                <Route path="/" exact component={TestSecondComponent}/>
                <Route path="/without" component={TestFirstComponent}/>
            </div>
        </Router>
    </Provider>

);

export default App;