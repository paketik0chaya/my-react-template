import React, {Component} from 'react';
import {connect} from "react-redux";
import {testAction} from "./redux/actions";
import {Link} from "react-router-dom";
import './style/index.css';
import photo from './photo.jpg';

class TestSecondComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            arrayTest: [1, 2, 3]
        }
    }

    componentDidMount() {
        this.props.testAction('Hello, world!')
    }

    render() {
        const {message} = this.props;
        const myArray = {...this.state.arrayTest};
        return (
            <div className="test_style">
                <div>
                    <Link to={'without'}>Component with redux, here's data string - {message}</Link>

                </div>
                <div>
                    <img src={photo} alt={"cat-in-hat"}/>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        message: state.testReducer.message,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        testAction: (text) => dispatch(testAction(text)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestSecondComponent);
