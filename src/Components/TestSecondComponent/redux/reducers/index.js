import * as constant from "../constants";

const initialState = {
    message: ''
};

export default function testReducer(state = initialState, action) {
    switch (action.type) {
        case constant.TEST_ACTION:
            return {
                message: action.message
            };
        default:
            return state
    }
}
