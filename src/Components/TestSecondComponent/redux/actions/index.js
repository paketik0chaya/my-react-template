import * as constant from "../constants";

export const testAction  = (text) => ({
    type: constant.TEST_ACTION,
    message: text,
});
